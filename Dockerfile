#mylinux
FROM fedora:27
LABEL author="Rafa.C"
RUN yum install -y procps nmap tree vim iputils iproute util-linux
WORKDIR /tmp
CMD /bin/bash