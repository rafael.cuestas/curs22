# curs22
Apunts del profe 
https://gitlab.com/edtasixm05/docker  
https://hub.docker.com/u/edtasixm05  

## Primer exemple de docker
Hola aquest es el primer ejemple de docker

## Trukis per a docker
### docker history
docker history <nom imatge> 
retorna el historial de la imatge les modificacions realitzades

## docker tag 
Afegeix una etiqueta a la imatge, 
## docker login/logout pull/push
S'autentifica al repositoris d'imatges
```
docker login
docker logout
```
Puja i vaixa una imatge
```
docker pull/push
```

## docker run
Inicia un container desde una imatge
```
docker run -it --name <tag_container> -h <nom_host> <nom_imatge>
docker run -it --rm # corre la imatge en un container i cuan es mor el container l'elimina
docker run -d #s'engega el container sense consola
```
## docker cp 
Copia fitcher del host al tag_container
``` 
docker cp <nom_container>:<ruta_fittxer> <ruta_local_fitxer>
```

## docker build 
Crear una imatge apartir del dockerfile
```
docker build -t <tag_imatge> . 
docker build -t rafaelcuestas/myweb:latest .
```
## docker exec 
Executa una comanda a un container
```
docker exec -it <nom_container> <comanda>
docker exec -it <nom_container> bash
```
## Publicar ports
Per a publicar el ports
```
docker run -d <nom_container> -p <ip>:<port_intern> <port_extern>
docker run --rm -p 80:80 -d rafaelcuestas/myweb
Publica un port dinamic apartir del 45000 i pico
docker run --rm -P -d rafaelcuestas/myweb
```
# DockerFile
Serveix per a definir les imatges  
FROM  
LABEL  
RUN  
COPY  
ADD  
WORKDIR  
CMD  
EXPOSE  
ENTRIPOINT  
ENV  

## Exposar directoris amb bind
Mitjançant docker run podem exposar un directori del host
```
docker run --rm -d <director_host>:<directori_container>
```
## Exposar directoris amb volumes
La diferencia entre un bind i un volum es un concepte lógic
```
docker volumes ls
docker volume create volum1
```

# Networks
iceupc@j03:/var/tmp/curs22$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
03e409593a3f   bridge    bridge    local
12dfc3624c60   host      host      local
3dc87bc658aa   none      null      local
bridge -> defecte no te resolusió de noms
host -> container nomes té conetivitat amb el host
none -> no esta connectat amb ningú

```
docker network ls 
docker network create netA
docker network create netB
docker network create \
  --subnet=127.25.0.0/24 \
  --gateway=127.25.0.1 \
  netC
```

La xarxa per defaul no te DNS. s'han de crear una network
```
docker network create netA
docker run --rm --name ssh.edt.org -h ssh.edt.org --net netA -d edtasixm05/ssh21 
docker run --rm --name host1.edt.org -h host1.edt.org --net netA -d edtasixm05/ssh21 
```

# CMD vs ENTRYPOINT

# Variable d'entorn 
Passar variables a l'instancia del container
En els Dockerfile es fa amb 
ENV nom=VALOR
```
export any=2022
docker run --rm -e any -e nom="pere" -it debian bash
#echo $any
```







































<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />















## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/rafael.cuestas/curs22.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/rafael.cuestas/curs22/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
